from scipy.io import loadmat
# data = loadmat('/home/tamnv/Downloads/data_landmark/land_mark/HELEN/HELEN_232194_1_0_pts.mat')
# print(data.keys())
#
# print (data['pts_2d'])
import json
import pickle
import cv2
# import cv
import os
import math
import numpy as np
from PIL import Image
from collections import OrderedDict
import cPickle as pickle
import string
import copy
import argparse

def box(rects, img, out_img):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), (127, 255, 0), 10)
    return img

reduced_size = (256, 256)
data = dict()
name_file = []
name_file_test = []
numtrain =int(0)
numtest = int(0)

def crop_img(img, x_start, y_start, x_end, y_end):
    # the method for cropping an image
    return img.crop((x_start, y_start, x_end, y_end))

def resize_img(img, size):
    # the methods for downsampling
    return img.resize(size, Image.ANTIALIAS)

def get_kpts(file_path):
    # kpts = []
    # f = open(file_path, 'r')
    # ln = f.readline()
    # while not ln.startswith('n_points'):
    #     ln = f.readline()
    #
    # num_pts = ln.split(':')[1]
    # num_pts = string.strip(num_pts)
    # # checking for the number of keypoints
    # if float(num_pts) != num_kpts:
    #     print "encountered file with less than %f keypoints in %s"
    #     return None
    #
    # # skipping the line with '{'
    # ln = f.readline()
    #
    # ln = f.readline()
    # while not ln.startswith('}'):
    #     vals = ln.split(' ')[:2]
    #     vals = map(string.strip, vals)
    #     vals = map(np.float32, vals)
    #     kpts.append(vals)
    #     ln = f.readline()
    data = loadmat(file_path)
    # print(data['pts_2d'].shape)
    return data['pts_2d']

def get_border_mask(kpts, img_width, img_height):
    # This module gets whether each keypoint touches the boundary,
    # for top and left boundaries if the values are in the range [0,10]
    # it is considered on the boundary
    # for bottom and right boundaries if they are within 3 pixels from the border
    # they are considered on the boundary
    # it returns a vector of size (#kpts), where for each keypoint
    # one of these 4 values is given:
    # 0: no_boundary, 1: left boundary, 2: top boundary
    # 3: right boundary, 4: bottom boundary
    flags = np.zeros(kpts.shape[0])

    # checking for left boundary
    left_bound = kpts[:,0] < 10
    flags += left_bound * 1

    # checking for top boundary
    top_bound = kpts[:,1] < 10
    flags += top_bound * 2

    # checking for right boundary
    right_bound = (img_width - kpts[:,0]) <= 3
    flags += right_bound * 3

    # checking for bottom boundary
    bottom_bound = (img_height - kpts[:,1]) <= 3
    flags += bottom_bound * 4
    return flags
# dieu chinh box thanh hinh vuong
def get_bbox(x1, y1, x2, y2):
    # This module makes the bounding box sqaure by
    # increasing the lower of the bounding width and height
    x_start = int(np.floor(x1))
    x_end = int(np.ceil(x2))
    y_start = int(np.floor(y1))
    y_end = int(np.ceil(y2))

    width = np.ceil(x_end - x_start)
    height = np.ceil(y_end - y_start)

    if width < height:
        diff = height - width
        x_start -=(np.ceil(diff/2.0))
        x_end +=(np.floor(diff/2.0))
    elif width > height:
        diff = width - height
        y_start -=(np.ceil(diff/2.0))
        y_end +=(np.floor(diff/2.0))

    width = x_end - x_start
    height = y_end - y_start
    assert width == height
    rect_init_square = [int(x_start), int(y_start), int(width), int(height)]
    max_margin = 0
    return (rect_init_square, max_margin)

def validate_bbox(x_start, x_end, y_start, y_end, img_width, img_height):
    x_start_extra = 0
    y_start_extra = 0
    x_end_extra = 0
    y_end_extra = 0
    if x_start < 0:
        x_start_extra = -x_start
    if y_start < 0:
        y_start_extra = -y_start
    if x_end > img_width:
        x_end_extra = x_end - img_width
    if y_end > img_height:
        y_end_extra = y_end - img_height

    max_margin = np.max((x_start_extra, y_start_extra, x_end_extra, y_end_extra))

    if max_margin > 0:
        x_start += max_margin
        y_start += max_margin
        x_end -= max_margin
        y_end -= max_margin

    width2 = int(x_end - x_start)
    height2 = int(y_end - y_start)
    x_start = int(x_start)
    y_start = int(y_start)
    x_end = int(x_end)
    y_end = int(y_end)
    assert x_start >= 0
    assert y_start >= 0
    assert x_end <= img_width
    assert y_end <= img_height
    assert height2 == width2
    rect_init_square = [x_start, y_start, width2, height2]
    return (rect_init_square, max_margin)

def enlarge_bbox(bbox, ratio=1.25):
    x_start, y_start, width, height = bbox
    x_end = x_start + width
    y_end = y_start + height
    assert width == height, "width %s is not equal to height %s" %(width, height)
    assert ratio > 1.0# , "ratio is not greater than one. ratio = %s" %(ration,)
    change = ratio - 1.0
    shift = (change/2.0)*width
    x_start_new = int(np.floor(x_start - shift))
    x_end_new = int(np.ceil(x_end + shift))
    y_start_new = int(np.floor(y_start - shift))
    y_end_new = int(np.ceil(y_end + shift))

    # assertion for increase lenght
    width = int(x_end_new - x_start_new)
    height = int(y_end_new - y_start_new)
    assert height == width
    max_margin = 0
    rect_init_square = [x_start_new, y_start_new, width, height]
    return (rect_init_square , max_margin)
# tang kich thuoc cua anh
def increase_img_size(img, bbox):
    # this method increases the bounding box size
    # if start and end values for the bounding box
    # go beyond the image size (from either side)
    # and in such a case gets the ratio of padded region
    # to the total image size (total_img_size = orig_size + pad)
    x_start, y_start, width_bbox, height_bbox = bbox
    x_end = x_start + width_bbox
    y_end = y_start + height_bbox
    height, width, _ = img.shape
    # print("h, w", height, width)

    x_extra_start = 0
    x_extra_end = 0
    y_extra_start = 0
    y_extra_end = 0

    if x_start < 0:
        x_extra_start = - x_start
    if x_end > width:
        x_extra_end = x_end - width
    if y_start < 0:
        y_extra_start = - y_start
    if y_end > height:
        y_extra_end = y_end - height
    # print("start")
    # print(x_start, y_start)
    # print("extra")
    # print(x_extra_start, y_extra_start)
    # print(x_extra_end, y_extra_end)


    # extending the image khi ma start bi lui ra phis ngoai 0 end ra ngoai chieu cao cua anh
    img_new = copy.copy(img)
    x_extra = x_extra_start + x_extra_end

    if x_extra > 0:
        img_new = np.zeros((height, width + x_extra, 3))
        #img_new[ :, : x_extra_start, :] = colbs
        img_new[ :, x_extra_start : x_extra_start + width, :] = img
        #img_new[ :, x_extra_start + width :  x_extra_start + width + x_extra_end, :] = coles

    width = img_new.shape[1]
    y_extra = y_extra_start + y_extra_end
    img_new2 = copy.copy(img_new)
    if y_extra > 0:
        img_new2 = np.zeros((height + y_extra, width, 3))
        #img_new2[ : y_extra_start, :, :] = rowbs
        img_new2[y_extra_start : y_extra_start + height, :, :] = img_new
        #img_new2[y_extra_start + height :  y_extra_start + height + y_extra_end, :, :] = rowes

    # getting the ration of the padded region to the total image size
    total_width = float(width_bbox)
    left_ratio = x_extra_start / total_width
    right_ratio = x_extra_end / total_width
    total_height = float(height_bbox)
    top_ratio = y_extra_start / total_height
    bottom_ratio = y_extra_end / total_height
    pad_ratio = [left_ratio, top_ratio, right_ratio, bottom_ratio]

    # checking bounding box size after image padding
    height, width, _ = img_new2.shape
    if x_extra_start:
        x_start = 0
        x_end += x_extra_start
    if y_extra_start:
        y_start = 0
        y_end += y_extra_start
    if x_extra_end:
        x_end = width
    if y_extra_end:
        y_end = height
    bbox_width = x_end - x_start
    bbox_height = y_end - y_start
    assert bbox_width == bbox_height

    rect = [x_start, y_start, bbox_width, bbox_height]

    #import pdb; pdb.set_trace()
    if y_extra or x_extra:
        print ("extended the image")

    return img_new2, rect, x_extra_start, y_extra_start, pad_ratio


def process_files(source_dir, folder, subset):
    X = []
    Y = []
    Y_img_name = []


    save_dir_file_out = "./data_full/big_data_process/image"
    # save_dir_out_test = "./data_full/big_data_process/image/test"
    source_landamark = "./data_full/big_data/landmarks"

    path_landmark = "%s/%s" %(source_landamark, folder)
    # print("path_landmark", path_landmark)
    source_image = './data_full/big_data/image'
    path_image = "%s/%s" % (source_image, folder)
    # path = "%s/%s" %(source_dir, folder)
    files = os.listdir(path_image)

    files = [i for i in files if i.endswith('.png') or i.endswith('.jpg')]
    # print(files)


    sanity = False# undownsampled image chacking
    check = False # downsampled image checking
    save = True
    for index, file_image in enumerate(files):

        file_path_image = "%s/%s/%s" %(source_image,folder, file_image)
        print("ten_file_anh", file_path_image)





        file_pts = file_image.split('.')[0] + '_pts.mat'
        # print('pts',file_pts)
        path_file_landmark =  "%s/%s" %(path_landmark, file_pts)
        print('pts',path_file_landmark)
        kpts = get_kpts(path_file_landmark)
        if kpts is None:
            continue
        print("file pts: ",path_landmark )
        Y_img_name.append(file_path_image)
        im = Image.open(file_path_image)# read image
        img_width, img_height = im.size # kich thuoc anh
        #print "width %s, height %s" %(img_width, img_height)

        # checking if the bounding box covers the whole face
        kpts = np.array(kpts)
        x_min = np.min(kpts[:,0])
        x_max = np.max(kpts[:,0])
        y_min = np.min(kpts[:,1])
        y_max = np.max(kpts[:,1])

        # detect the initial bbox using only the min and max values of the keypoints
        bbox, max_margin = get_bbox(x_min, y_min, x_max, y_max)
        border_mask = get_border_mask(kpts, img_width, img_height)
        # enlarge the bbox by a ratio
        rect, max_margin = enlarge_bbox(bbox, ratio=1.7)

        x_start, y_start, width, height = rect
        x_end = x_start + width
        y_end = y_start + height

        assert x_start < x_min, "x_state %s is not smaller than x_min %s" %(x_start, x_min)
        assert x_end > x_max, "x_end %s is not bigger than x_max %s" %(x_end, x_max)
        assert y_start < y_min, "y_state %s is not smaller than y_min %s" %(y_start, y_min)
        assert y_end > y_max, "y_end %s is not bigger than y_max %s" %(y_end, y_max)

        ###################
        # sanity checking #
        ###################
        """
        Note: the sanity section (if set to true) writes to an output file the detected rectangle and the keypoint locations on them
              to verify the keypoint locations and the rectangle are correct.
        """
        if sanity:
            source_dir = parent_dir + '/created_datasets' + '/kpt_detection_raw'
            if not os.path.exists(source_dir):
                try:
                    os.makedirs(source_dir)
                except OSError:
                    if not os.path.isdir(source_dir):
                        raise

            sub_dir = source_dir + '/' + subset
            if not os.path.exists(sub_dir):
                try:
                    os.makedirs(sub_dir)
                except OSError:
                    if not os.path.isdir(sub_dir):
                        raise

            san_dir = sub_dir + '/' + folder
            if not os.path.exists(san_dir):
                try:
                    os.makedirs(san_dir)
                except OSError:
                    if not os.path.isdir(san_dir):
                        raise

            out_file_path = san_dir + '/' + file_image

            #####################################
            # converting from PIL to cv2 format #
            #####################################
            # in order to convert PIL image to cv2 format, the image should be converted to RGB first
            img_rgb = im.convert('RGB')
            # getting the numpy array from image, this numpy array is in RGB format
            img_npy=np.asarray(img_rgb)
            # cv2 images are in BGR fortmat, so RGB should be changed to BGR
            if len(img_npy.shape) == 3:
                img_npy = img_npy[:, :, ::-1].copy()
            # if the image is in gray-scale convert it to BGR
            elif len(img_npy.shape) == 2:
                img_npy = cv2.cvtColor(img_npy, cv2.COLOR_GRAY2BGR)

            #increasing the image size
            img_npy, rect, x_extra_start, y_extra_start, pad_ratio = increase_img_size(img_npy, rect)
            x_start, y_start, width, height = rect
            x_end = x_start + width
            y_end = y_start + height
            img_height, img_width, _ = img_npy.shape
            # neu mo rong theo chieu ngang thi cogn voi gia tri mo rong them mot luong
            if x_extra_start:
                kpts[:,0] += x_extra_start
            if y_extra_start:
                kpts[:,1] += y_extra_start

            for kpt in kpts:
                c, r = kpt[1], kpt[0]
                if r < img_width and c < img_height:
                    if len(img_npy.shape) == 3:
                        #img_npy[c,r] = (0,255,0)
                        cv2.circle(img_npy, (r,c), 2, (0,255,0))
                    else: # it has a len of 2
                        #img_npy[c,r] = 1.0 # filling with white value
                        cv2.circle(img_npy, (r,c), 2, 1.0)

            rects = [[x_start, y_start, x_end, y_end]]
            img_out = box(rects, img_npy, out_file_path)
            print('out_file_sanity',out_file_path)
            cv2.imwrite(out_file_path, img_out)


            #import pdb; pdb.set_trace()
        #######################
        # end sanity checking #
        #######################

        #########################################
        # getting the train and test datasets   #
        #########################################
        else:
            img = Image.open(file_path_image)
            #####################################
            # converting from PIL to cv2 format #
            #####################################
            # in order to convert PIL image to cv2 format, the image should be converted to RGB first
            img_rgb = img.convert('RGB')
            # getting the numpy array from image, this numpy array is in RGB format
            img_npy=np.asarray(img_rgb)
            # cv2 images are in BGR fortmat, so RGB should be changed to BGR
            if len(img_npy.shape) == 3:
                img_npy = img_npy[:, :, ::-1].copy()
            # if the image is in gray-scale convert it to BGR
            elif len(img_npy.shape) == 2:
                img_npy = cv2.cvtColor(img_npy, cv2.COLOR_GRAY2BGR)

            #increasing the image size
            img_npy, rect, x_extra_start, y_extra_start, pad_ratio = increase_img_size(img_npy, rect)
            x_start, y_start, width, height = rect
            x_end = x_start + width
            y_end = y_start + height
            img_height, img_width, _ = img_npy.shape

            if x_extra_start:
                kpts[:,0] += x_extra_start
            if y_extra_start:
                kpts[:,1] += y_extra_start

            #####################################
            # converting from cv2 to PIL format #
            #####################################
            #img = Image.fromstring("RGB", cv.GetSize(img_npy), img_npy.tostring())
            # converting from BGR to RGB
            img_RGB = img_npy[:, :, ::-1].copy()
            # making sure values are uint8
            img_RGB=np.uint8(img_RGB)
            img = Image.fromarray(img_RGB)

            # cropping the image
            cropped_img = crop_img(img, x_start, y_start, x_end, y_end)

            # downsampling image
            resized_img = resize_img(cropped_img, size=reduced_size)

            #####################################
            # converting from PIL to cv2 format #
            #####################################
            # in order to convert PIL image to cv2 format, the image should be converted to RGB first
            img_rgb = resized_img.convert('RGB')
            # getting the numpy array from image, this numpy array is in RGB format
            img_npy=np.asarray(img_rgb)
            # cv2 images are in BGR fortmat, so RGB should be changed to BGR
            if len(img_npy.shape) == 3:
                img_npy = img_npy[:, :, ::-1].copy()
            # if the image is in gray-scale convert it to BGR
            elif len(img_npy.shape) == 2:
                img_npy = cv2.cvtColor(img_npy, cv2.COLOR_GRAY2BGR)

            # normalizing the keypoint locations after cropping and downsampling
            # to a range of [0,1]
            width = height = x_end - x_start
            # getting the normalized keypoints
            kpts_norm = np.zeros_like(kpts)
            kpts_norm[:,0] = kpts[:,0] - x_start
            kpts_norm[:,1] = kpts[:,1] - y_start
            kpts_norm[:,0] /= width
            kpts_norm[:,1] /= height
            #import pdb; pdb.set_trace()

            if check:
                kpts_checks_sub = kpts_norm.copy()
                kpts_checks_sub *= reduced_size[0]
                img_check = img_npy.copy()
                for kpt in kpts_checks_sub:
                    c, r = kpt[1], kpt[0]
                    if r < img_width and c < img_height:
                        if len(img_check.shape) == 3:
                            img_check[int(c),int(r)] = 255
                            cv2.circle(img_check, (r,c), 1, (0,255,0))
                        else: # it has a len of 2
                            img_check[int(c),int(r)] = 1.0 # filling with white value
                            cv2.circle(img_check, (r,c), 1, 1.0)

                source_dir = parent_dir + '/created_datasets' + '/kpt_detection_downsampled'
                if not os.path.exists(source_dir):
                    try:
                        os.makedirs(source_dir)
                    except OSError:
                        if not os.path.isdir(source_dir):
                            raise
                sub_dir = source_dir + '/' + subset
                if not os.path.exists(sub_dir):
                    try:
                        os.makedirs(sub_dir)
                    except OSError:
                        if not os.path.isdir(sub_dir):
                            raise
                san_dir = sub_dir + '/' + folder
                if not os.path.exists(san_dir):
                    try:
                        os.makedirs(san_dir)
                    except OSError:
                        if not os.path.isdir(san_dir):
                            raise

                out_file_path = san_dir + '/' + file_image
                print("check",out_file_path)

                cv2.imwrite(out_file_path, img_check)

            # X.append(img_npy)
            # Y_image_name.append(file_path_image)
            kpts_checks_sub = kpts_norm.copy()
            kpts_checks_sub *= reduced_size[0]
            if save:
                file_path = save_dir_file_out+"/"+ file_image
                if (file_image.split('_')[2] == 'test' and len(name_file_test) < 2000):
                    name_file_test.append(file_path)


                else:
                    name_file.append(file_path)

                    # print(file_path)
                    # print("test")
                # cv2.imwrite(file_path, img_npy)
                data[file_path] = kpts_checks_sub


        print ("done with index",index ,"file", file_image)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Creating the pickle files for 300W datasets.')
    # parser.add_argument('--src_dir', type=str, help='the complete path to the folder containing Train_set and Test_set folders', required=True)
    # parser.add_argument('--dest_dir', type=str, help='the complete path to the folder where the created pickle files with be saved', required=True)
    #
    # args = parser.parse_args()
    num_kpts = 68

    parent_dir = "./data_full/big_data/image"
    train_dir ="./data_full/big_data/image"
    out_dir = "./data_full/big_data_process"
    # test_dir = "%s/Test_set" %(parent_dir)

    X_train = []
    X_test = []
    Y_train = OrderedDict()


    Y_landmark_train = []
    Y_landmark_test = []


    # going through the train set
    subdirs = os.listdir(train_dir)
    subdirs = [i for i in subdirs if not i.startswith('.')]
    for subdir in subdirs:
        process_files(train_dir, subdir, 'Train_set')
    #save landmark
    data_name_file = {}
    data_name_file_test ={}
    data_name_file_test['name'] = name_file_test
    data_name_file['name'] = name_file
    with open('./data_full/big_data_process/data_landmark.pickle', 'w') as outfile:
        pickle.dump(data, outfile)
    with open('./data_full/big_data_process/name_file_train.txt','w') as outfile:
        json.dump(data_name_file, outfile)
    with open('./data_full/big_data_process/name_file_test.txt','w') as outfile:
        json.dump(data_name_file_test, outfile)




    #     print("shape x", np.array(x).shape)
    #     size_train =int(len(x)/5)
    #     X_test.extend(x[:size_train][:][:][:])
    #     Y_landmark_test.extend(y_landmark[:size_train][:][:])
    #     X_train.extend(x[size_train:][:][:][:])
    #     Y_landmark_train.extend(y_landmark[size_train:][:][:])


    # # creating an OrderedDict for the train set
    # train_set = OrderedDict()
    # test_set = OrderedDict()
    # # Y_train = Y_landmark
    # # number_train = 90
    # train_set['X'] = X_train
    # train_set['Y'] = Y_landmark_train
    # test_set['X'] = X_test
    # test_set['Y'] = Y_landmark_test


    # train_pkl_path = out_dir + '/300W_train_thu.pickle'
    # with open(train_pkl_path, 'wb') as fp:
    #     pickle.dump(train_set, fp)
    # test_pkl_path = out_dir+'/300W_test_thu.pickle'
    # with open(test_pkl_path,'wb') as fp:
    #     pickle.dump(test_set, fp)

#/home/tamnv/Downloads/data_landmark/created_datasets/kpt_detection_raw/Train_set/HELEN/HELEN_15451707_1_17.jpg
