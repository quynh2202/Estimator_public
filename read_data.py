import pickle
import numpy as np
def readata(path_file):
    print("reading_data")
    file = open(path_file, "rb")
    data = pickle.load(file)
    image = data['X']
    label = data['Y']
    label = np.array(label)


    out_data = []
    image = np.asarray( image) / 255.0
    for i in range(len(image)):
        out_data.append((image[i], label[i]))
    print("shape", image.shape)
    print("done")
    return out_data
# path_file = "/home/dsvn/quynhpt/Estimator/data/300W_process/300W_train_256by256.pickle"
# readata(path_file)
