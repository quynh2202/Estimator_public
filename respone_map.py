import numpy as np
import math
import string
import read_data
import tensorflow as tf
from matplotlib import pyplot as mp
import matplotlib.pyplot as plt
import math
# np.randoeum.normal([1,2,3,4,5], 2, )
mu, sigma = 0, 0.1
s = np.random.normal(mu, sigma, 2)

def gaussian(bach_size, y, size=256):
    prob = []
    # arr_tesor = tf.zeros(shape = (bach_size, int(y.get_shape()[-1]), size, size,2), dtype=tf.float32)
    arr = np.zeros(shape=(size, size, 2), dtype=float)
    for i in range(int(size)):
        for j in range(size):
            arr[i][j] = [float(i), float(j)]
    a = tf.constant(value=arr, dtype=tf.float32)
    rate = size/256.0
    print("rate",rate)
    ma = rate *6.0
    print("ma", ma)
    y_sau = y  * rate
    for k in range(bach_size):
        print("anh thu ",k)

        arr_tesor = []
        for i in range(68):
            dist = tf.contrib.distributions.MultivariateNormalDiag(loc=y_sau[k][i], scale_diag=[ma, ma])
            arr_tesor.append(dist.prob(a))
        prob.append(arr_tesor)
    prob = tf.transpose(prob, perm=[0, 3, 2, 1])
    return prob
def gauss_test_demension(bach_size, y, weight= 256, height = 256):
    prob = []
    # arr_tesor = tf.zeros(shape = (bach_size, int(y.get_shape()[-1]), size, size,2), dtype=tf.float32)
    arr = np.zeros(shape=(weight, height, 2), dtype=float)
    for i in range(int(height)):
        for j in range(int(weight)):
            arr[i][j] = [float(i), float(j)]
    a = tf.constant(value=arr, dtype=tf.float32)
    rate = height/ 256.0
    print("rate",rate)
    ma = rate *6.0
    print("ma", ma)
    y_sau = y  * rate
    for k in range(bach_size):
        print("anh thu ",k)

        arr_tesor = []
        for i in range(68):
            dist = tf.contrib.distributions.MultivariateNormalDiag(loc=y_sau[k][i], scale_diag=[ma, ma])
            arr_tesor.append(dist.prob(a))
        prob.append(arr_tesor)
    prob = tf.transpose(prob, perm=[0, 3, 2, 1])
    return prob
def my_gauss(img, label):
    ma = 6.0
    out_put = []
    for i in range(68):
        result = np.zeros(shape=(256, 256), dtype=float)
        for row in range(256):
            for cow in range(256):
                mu =  -((row-label[i][1])*(row-label[i][1]) + (cow-label[i][0])*(cow-label[i][0]))/(2.0*ma*ma)
                result[row][cow] = 1/(2.0*math.pi* ma*ma)*(math.exp(mu))
        out_put.append(result)
    out_put = np.transpose(out_put, axes=[1,2,0])
    # out_put = np.concat(out_put)
    return out_put
# data = read_data.readata("/home/tamnv/Downloads/300W_process/300W_train_256by256.pickle")
# for i in range(3,4):
#     img = data[i][0]
#     label = data[i][1]
# out_put = my_gauss(img, label)
# print(out_put[0][int(label[0][1])][int(label[0][0])])
# print(out_put)



    # prob = []
    # # arr_tesor = tf.zeros(shape = (bach_size, int(y.get_shape()[-1]), size, size,2), dtype=tf.float32)
    # arr = np.zeros(shape=(size, size, 2), dtype=float)
    # for i in range(int(size)):
    #     for j in range(size):
    #         arr[i][j] = [float(i), float(j)]
    # a = tf.constant(value=arr, dtype=tf.float32)
    # y =  y/4
    # rate = size/256.0
    # y = y  * rate
    # ma = 6.0* rate
    # ma =1.5

    # for k in range(bach_size):

    #     arr_tesor = []
    #     for i in range(68):
    #         dist = tf.contrib.distributions.MultivariateNormalDiag(loc=y[k][i], scale_diag=[6., 6.])
    #         arr_tesor.append(dist.prob(a))
    #     prob.append(arr_tesor)
    # prob = tf.transpose(prob, perm=[0, 2, 3,1])
    # return prob
def gauss_test():
    data = read_data.readata("/home/tamnv/Downloads/300W_process/300W_train_256by256.pickle")
    img = []
    label = []
    for i in range(3,4):
        img.append(data[i][0])
        label.append(data[i][1])
    y1 = tf.placeholder(tf.float32, [None, 68, 2])
    result_dau= gaussian(1, y1, 256)
    sess = tf.Session()
    w = int(label[0][0][0] )
    h = int(label[0][0][1] )
    result = sess.run(result_dau, feed_dict={ y1:label})
    ma = 6.0

    for row in range(256):
        for cow in range(256):
            mu =  -((row-label[0][0][1])*(row-label[0][0][1]) + (cow-label[0][0][0])*(cow-label[0][0][0]))/(2.0*ma*ma)
            a = 1/(2.0*math.pi* ma*ma)*(math.exp(mu))
            if (result[0][row][cow][0] > 1e-4 ):
                print(result[0][row][cow][0], a)
    # print("result",result)

    # result = np.transpose(result, axes=[0, 3, 1, 2])
    # # b = np.array(result)
    # # print("shape",result.shape)
    # img[0][int(label[0][10][1]),int(label[0][10][0])] = 0
    # plt.imshow(img[0])
    # plt.show()
    # plt.imshow(result[0][10])
    # plt.show()
    # plt.imshow(result_dau[0][0])
    # plt.show()
    # plt.imshow(result_dau[0][10])
    # plt.show()
    # plt.imshow(result_dau[0][61])
    # plt.show()
    # plt.imshow(result_dau[0][50])
    # plt.show()
    # plt.imshow(result_dau[0][40])
    # plt.show()
    # print(label)
    # print(y_sau)
    # print(np.array(result_dau).shape)
    # print("diem gan landmark")
    # print(result_dau[0][h-4:h+4][w-4:w+4][0])
# gauss_test()
# def gau_dung():
#     data = read_data.readata("/home/tamnv/Downloads/300W_process/300W_train_256by256.pickle")
#     img = []
#     label = []
#     for i in range(3,4):
#         img.append(data[i][0])
#         label.append(data[i][1])
#     print(label)
#     l = label
#     y1 = tf.placeholder(tf.float32, [None, 68, 2])
#     for i in range(68):
#         tg = label[0][i][1]
#         label[0][i][1]= label[0][i][0]
#         label[0][i][0] = tg
#     y1 = tf.placeholder(tf.float32, [None, 68, 2])
#     y2 = tf.placeholder(tf.float32, [None, 68, 2])
#     result_dau = gaussian(1, y1, 256)
#     result = tf.transpose(result_dau, perm=[0, 1,3,2])
#     result_sau = gaussian(1,y2, 256)
#     # result_sau = tf.transpose(result_sau, perm=[0,2,3,1])
#     # result = tf.transpose(result, perm=[0,2,3,1] )

#     # m = tf.reduce_max(result, reduction_indices = [0,1,2,3])
#     sess = tf.Session()
#     w = int(label[0][0][0] )
#     h = int(label[0][0][1] )
#     result_dau, result_sau = sess.run([result, result_sau], feed_dict={ y1: l, y2: label})
#     # print(result_dau- result_sau)
#     # assert False

#     print(np.array(result).shape)
#     # print("vtr max",tf.argmax(input = result, axis = 0  ))

#     print("diem gan landmark",result[0][h][w][0], " ", result[0][h- 1][w ][0], " ", result[0][h + 1][w ][0])
# gau_dung()
