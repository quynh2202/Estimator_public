import tensorflow as tf
import cv2
# import read_data
import read_image
import respone_map as res_map
# import Input_estimantor
import numpy as np
from scipy.io import savemat
import os
import pickle
# import test_record as rd
import matplotlib.pyplot as plt
import json
class FCN_net():
    def __init__(self, train_data=None, valid_data=None, test_data=None, learning_rate=1e-10, train=True, momentum=0.95,
                 size_input=(256, 256), num_class=68, batch_size=1, epoch=20, num_chanel=3, num_land=68, max_to_keep=5,
                 current_save_folder='./save_decv_rate_v3_RMS', valid_save_folder='./save/best_valid/',
                 last_save_folder='./save/last/', logs_folder='./summary/'):
        self.lr = learning_rate
        self.mmt = momentum
        self.data_train = train_data
        self.test_data = test_data
        self.valid_data = valid_data
        self.size_input = size_input
        self.num_class = num_class
        self.batch_size = batch_size
        self.epoch = epoch
        self.num_chanel = num_chanel
        self.num_land = num_land
        self.current_save_folder = current_save_folder
        self.valid_save_folder = valid_save_folder
        self.last_save_path = last_save_folder
        self.log_folder = logs_folder
        self.max_to_keep = max_to_keep
        self.global_step = tf.contrib.framework.get_or_create_global_step()
        self.weight_decay = 1e-4
        self.size_input = size_input
        self._input()

        self.inference()

        self.losses()
        self.train_step()
        self.count_trainable_params()
        self.init_session()

    def _input(self):
        self.is_traning = tf.placeholder(tf.bool)
        self._x = tf.placeholder(tf.float32, [None, self.size_input[0], self.size_input[1], self.num_chanel])
        tf.summary.image("Input", self._x, 3)
        self._y = tf.placeholder(tf.float32, [None, self.num_land, 2])
    def bias(self, x, name=None):
        shape = x.get_shape()[-1]
        a = tf.constant(0.0, dtype = tf.float32, shape = [shape])

        return tf.get_variable(initializer= a, name=name )

    def conv2d(self, x, out_filter, kernel_size, padding="SAME", name=None):
        in_filter = int(x.get_shape()[-1])
        # filters = tf.get_variable( name="DW"+str(name), shape=[kernel_size,
        filters = tf.get_variable(name="DW" + str(name),
                                  dtype=tf.float32,
                                  initializer=tf.truncated_normal(shape=[kernel_size, kernel_size, in_filter, out_filter] , stddev= 0.01, mean=0.0))
        tf.summary.histogram("DW"+str(name), filters)
        out_put = tf.nn.conv2d(input=x, filter=filters, strides=[1, 1, 1, 1], padding=padding)
        tf.summary.histogram("con2"+str(name), out_put)
        bias = self.bias(x = out_put, name ="bias_"+name)
        tf.summary.histogram("bias"+str(name), bias)
        out_put = tf.nn.bias_add(out_put, bias)
        tf.summary.histogram("layer"+str(name), out_put)
        return out_put

    def conv2d_dilat(self, x, out_filter, kernel_size, padding="SAME", dilation=1, name=None):

        in_filter = int(x.get_shape()[-1])

        filters = tf.get_variable(name="DW" + str(name),
                                  dtype=tf.float32,
                                  initializer=tf.truncated_normal( shape=[kernel_size, kernel_size, in_filter, out_filter], stddev=0.01, mean= 0.0))
        tf.summary.histogram("DW-dila"+str(name), filters)
        out_put = tf.nn.atrous_conv2d(value=x, filters=filters, rate=[dilation, dilation], padding=padding)
        tf.summary.histogram("dila"+str(name), out_put)
        bias = self.bias(x=out_put, name="bias_"+name)
        tf.summary.histogram("bias-dila"+str(name), bias)
        out_put = tf.nn.bias_add(out_put, bias)
        tf.summary.histogram("layer_dila", out_put)
        return out_put


    def max_pooling(self, x, kernel_size, padding="VALID", stride=2):
        return tf.nn.max_pool(value=x, ksize=kernel_size, strides=(1, stride, stride, 1), padding=padding)

    def relu(self, x):
        return tf.nn.relu(features=x)
    def leaky_relu(x, leakiness=0.0):
        return tf.nn.leaky_relu(x, leakiness)

    def batch_norm(self, x, n_out, phase_train=True, scope='bn'):
        """
        Batch normalization on convolutional maps.
        Args:
            x:           Tensor, 4D BHWD input maps
            n_out:       integer, depth of input maps
            phase_train: boolean tf.Varialbe, true indicates training phase
            scope:       string, variable scope
        Return:
            normed:      batch-normalized maps
        """

        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
                               name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
                                name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.5)

            def mean_var_with_update():
                ema_apply_op = ema.apply([batch_mean, batch_var])
                with tf.control_dependencies([ema_apply_op]):
                    return tf.identity(batch_mean), tf.identity(batch_var)

            mean, var = tf.cond(phase_train,
                                mean_var_with_update,
                                lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-3)
        return normed

    def dilated_layer(self, x, out_filter, list_kernel_size=(3, 3, 3, 3), list_dilate=(1, 2, 3, 4), number_block=4,
                      name=None):

        sub_out_filter = int(out_filter / number_block)
        # list_feature = self.conv2d(x, sub_out_filter, kernel_size = list_kernel_size[0], name="first_dilated"+ name )
        list_tensor_dilate = []

        for i in range(0, number_block):

            dilation = list_dilate[i]
            kernel_size = list_kernel_size[i]
            feature = self.conv2d_dilat(x, sub_out_filter, kernel_size=kernel_size, dilation=dilation,
                                        name="dialated" + str(i) + name)
            feature = self.relu(feature)
            list_tensor_dilate.append(feature)
        list_feature = tf.concat(list_tensor_dilate, axis=3)
        return list_feature

    def bn(self, tensor, name=None):
        return tf.nn.lrn(tensor, depth_radius=5, bias=2, alpha=1e-4, beta=0.75, name=name)

    def primaryNet(self):
        out_put = self._x
        with tf.variable_scope("primary_net"):
            with tf.variable_scope("Conv_pr_1" ):
                out_put = self.conv2d(x=out_put, out_filter= 128, kernel_size=5, padding="SAME",name="primary_1")
                out_put = self.relu(out_put)

            with tf.variable_scope("max_pooling_1"):
                out_put = self.max_pooling(x=out_put, kernel_size=(1, 2, 2, 1), stride=2)
            with tf.variable_scope("Conv_pr_2"):
                out_put = self.conv2d(x = out_put, out_filter= 128, kernel_size= 5, padding="SAME", name="primary_2")
                out_put = self.relu(out_put)
            with tf.variable_scope("max_pooling_2"):
                out_put = self.max_pooling(x=out_put, kernel_size=(1,2,2,1), stride=2)

            # conv3
            with tf.variable_scope("Conv_pr_3"):
                out_put = self.conv2d(x=out_put, out_filter=128, kernel_size=5, padding="SAME", name="primary_3")
                out_put = self.relu(out_put)
            conv3 = out_put
            #conv4

            with tf.variable_scope("Conv_pr_4"):
                out_put = self.dilated_layer(x=out_put, out_filter=512, name='4')

            with tf.variable_scope("Conv_pr_5"):
                out_put = self.dilated_layer(x=out_put, out_filter=1024, name="5")
            with tf.variable_scope("Conv_pr_6"):
                out_put = self.conv2d(x=out_put,out_filter=512, kernel_size=1,name= "primary_6" )
                out_put = self.relu(out_put)
            with tf.variable_scope("Conv_pr_7"):
                out_put = self.conv2d(x=out_put, out_filter=256,kernel_size=1, name='primary_7')
                out_put = self.relu(out_put)
            conv7 = out_put
            with tf.variable_scope("Conv_8"):
                out_put = self.conv2d(x=out_put, out_filter=68, kernel_size=1, name="primary_8")

        self.score_primary = out_put

        return conv3, conv7

    def FusionNet(self, x):
        with tf.variable_scope("Fusion_net"):
            with tf.variable_scope("conv1_fusion"):
                out_put = self.dilated_layer(x=x, out_filter=192, list_kernel_size=(3, 3, 3), number_block=3,
                                             name="first_fusion")

            with tf.variable_scope("conv_fusion_2"):
                out_put = self.dilated_layer(x=out_put, out_filter=256, list_kernel_size=(3, 3, 3, 5),
                                             list_dilate=(1, 2, 4, 3), name="2")
            with tf.variable_scope("conv_fusion_3"):
                out_put = self.dilated_layer(x=out_put, out_filter=512, list_kernel_size=(3,3,3,5), list_dilate=(1,2,4,3), name='3')

            with tf.variable_scope("conv_fusion_4"):
                out_put = self.conv2d(x=out_put, out_filter=256, kernel_size=1, name="conv_fusion")
                out_put = self.relu(out_put)
            with tf.variable_scope("conv_fusion_5"):
                out_put = self.conv2d(x=out_put, out_filter=68, kernel_size=1, name="conv_fusion_last")

        self.score_fusion = out_put
        return out_put

    def deconv(self, x, size, stride, out_chanel, output_shape=None):
        in_chanel = int(x.get_shape()[-1])
        filter = tf.get_variable("w_trans" + str(size),
                                 initializer=tf.truncated_normal([size, size, out_chanel, in_chanel], mean=0.0,
                                                                 stddev=0.01, dtype=tf.float32))
        # tf.summary.histogram("")
        out_put = tf.nn.conv2d_transpose(value=x, filter=filter, output_shape=output_shape,
                                         strides=[1, stride, stride, 1], padding="SAME")
        return out_put

    def UpsampleNet(self, x):
        with tf.variable_scope("up_sample_net"):
            with tf.variable_scope("upsample_1"):
                size = 1
                stride = 1
                out_chanel = 128
                # x_cv = tf.convert_to_tensor(value=x, dtype= tf.float32)
                out_shape = (self.bach_size, 64, 64, 128)
                upsample1 = self.deconv(x=x, size=size, stride=stride, out_chanel=out_chanel, output_shape=out_shape)

            # upsamlpe2
            with tf.variable_scope("upsample_2"):
                size2 = 9
                stride2 = 2
                out_chanel2 = 128
                out_shape2 = [int(upsample1.get_shape()[0]), 128, 128, 128]
                upsample2 = self.deconv(x=upsample1, size=size2, stride=stride2, out_chanel=out_chanel2,
                                        output_shape=out_shape2)
            # upsample3
            with tf.variable_scope("up_sample_3"):
                size3 = 5
                out_chanel3 = 68
                stride3 = 2
                out_shape3 = [int(upsample2.get_shape()[0]), 256, 256, 68]
                upsample3 = self.deconv(x=upsample2, size=size3, stride=stride3, out_chanel=out_chanel3,
                                        output_shape=out_shape3)
        self.score_upsample = upsample3
        return upsample3

    def UpsampleNet_v2(self, x):
        with tf.variable_scope("upsample_net"):
            size = 8
            stride = 4
            out_chanel = 68
            out_shape = (self.batch_size, self.size_input[0], self.size_input[1], 68)
            out_put = self.deconv(x=x, size=size, stride=stride, out_chanel=out_chanel,
                                              output_shape=out_shape)
            tf.summary.histogram("decnv", out_put)
            self.score_upsample = out_put
        return self.score_upsample
    def UpsampleNet_v3(self, x):
        with tf.variable_scope("up_sample_net"):
            size = 256
            out_put = tf.image.resize_images(x, [size, size])
        tf.summary.histogram("decnv", out_put)
        self.score_upsample = out_put
        return out_put
    def inference(self):
        conv3, conv7 = self.primaryNet()
        conv_3_7 = tf.concat(values=(conv3, conv7), axis=3)
        out_put = self.FusionNet(x=conv_3_7)
        out_put = self.UpsampleNet_v3(x=out_put)
        return out_put

    def losses(self):
        print("start_loss")
        self.heat_map_primary = res_map.gaussian(self.batch_size, self._y, self.size_input[0]/4)
        diff1 = self.score_primary - self.heat_map_primary
        # output = {"heat_map_primary": heat_map_primary}
        # savemat("primary.mat", output)
        self.loss_primary = tf.nn.l2_loss(diff1)

        diff2 = self.score_fusion - self.heat_map_primary
        self.loss_fusion = tf.nn.l2_loss(diff2)

        self.res_map_batch = res_map.gaussian(self.batch_size, self._y, self.size_input[0])
        diff3 = self.score_upsample - self.res_map_batch
        self.loss_up_sample = tf.nn.l2_loss(diff3)
        # self.l2_loss = tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
        self.l2_loss = []
        for var in tf.trainable_variables():
            if var.op.name.find('DW') > 0:
                self.l2_loss.append(tf.nn.l2_loss(var))
        self.l2_loss = self.weight_decay * tf.add_n(self.l2_loss)
        # self.total_loss = self.loss_up_sample +self.l2_loss
        self.total_loss = self.loss_fusion + self.loss_primary + self.loss_up_sample + self.l2_loss
        print("stop_loss")

    def train_step(self):
        print("start_step")
        self.train_step = tf.train.RMSPropOptimizer(learning_rate=self.lr, momentum=self.mmt).minimize(self.total_loss,
                                                                                                        global_step=self.global_step)
        # self.train_step = tf.train.MomentumOptimizer(learning_rate=self.lr, momentum=self.mmt, use_nesterov= True).minimize(self.total_loss,
        #                                                                                                 global_step=self.global_step)

    # self.train_step2 = tf.train.MomentumOptimizer(learning_rate = self.lr, momentum = self.mmt).minimize(self.loss_up_sample, global_step = self.global_step)
    # self.train_step3 = tf.train.MomentumOptimizer(learning_rate = self.lr, momentum = self.mmt).minimize(self., global_step = self.global_step)
    # ssert False      print("stop_step")

    def init_session(self):
        print("init_session")
        # tao folder de luu model
        if not os.path.isdir(self.current_save_folder):
            os.mkdir(self.current_save_folder)
            os.mkdir(os.path.join(self.current_save_folder, 'current'))
        # tao folder de luu log
        if not os.path.isdir(self.log_folder):
            os.mkdir(self.log_folder)
        #
        self.current_save_folder = os.path.join(self.current_save_folder, 'current') + '/'
        self.sess = tf.InteractiveSession()
        self.saver = tf.train.Saver(max_to_keep=self.max_to_keep)
        if not os.path.isfile(self.current_save_folder + 'model.ckpt.index'):
            print('Create new model')

            self.sess.run(tf.global_variables_initializer())
            print('OK')
        else:
            print('Restoring existed model')
            self.saver.restore(self.sess, self.current_save_folder + 'model.ckpt')
            print('OK')
            print(self.global_step.eval())
        tf.summary.histogram("score_fusion", self.score_fusion)
        tf.summary.histogram("score_primary", self.score_primary)
        tf.summary.histogram("score_upsample", self.score_upsample)
        tf.summary.scalar("loss", self.total_loss)
        self.merge = tf.summary.merge_all()
        writer = tf.summary.FileWriter
        self.graph = writer("./save_decv_rate_v3_RMS/vizual")
        self.graph.add_graph(self.sess.graph)

    def count_trainable_params(self):
        total_parameters = 0
        for variable in tf.trainable_variables():
            shape = variable.get_shape()
            variable_parametes = 1
            for dim in shape:
                variable_parametes *= dim.value
            total_parameters += variable_parametes
        print("Total training params: %.1fM" % (total_parameters / 1e6))

    def train(self, num_epoch):
        print("start_train")
        with open("./data_full/big_data_process/data_landmark.pickle","r") as out_file:
            data_lm = pickle.load(out_file)
        with open("./data_full/big_data_process/name_file_train.txt","r") as out_file:
            file_names = json.load(out_file)
        # file_names = file_names[:1000]
        number_image= len(file_names)
        print("number_image_train", number_image)
        number_batch =int(number_image /self.batch_size)
        current_epoch = int(self.global_step.eval() / number_batch)

        for i in range(current_epoch, num_epoch + 1):
            print("Epoch ", i)

            np.random.shuffle(file_names)
            sum_loss = []
            up_loss = []

            for j in range(number_batch):
                top = int(j * (self.batch_size))
                bottom = int(min((j + 1) * (self.batch_size), number_image))
                while (bottom - top < self.batch_size):
                    top = top - 1
                img_batch, label_batch = read_image.read_image(file_names[top:bottom], data_lm)
                img_batch = np.asarray(img_batch)
                label_batch = np.asarray(label_batch)

                # img_batch, label_batch = Input_estimantor.augumentation(img_batch, label_batch)
                # print(img_batch.shape)
                # print("label_batch")
                # print(label_batch.shape)
                merge , ttl,_,up, l2 = self.sess.run(
                    [self.merge, self.total_loss, self.train_step,
                     self.loss_up_sample, self.l2_loss],
                    feed_dict={self._x: img_batch, self._y: label_batch, self.is_traning: True})
                sum_loss.append(ttl)
                up_loss.append(up)
                self.graph.add_summary(merge, i*number_batch+j )
                print("Training batch ", j, " Loss ", ttl, "-------------ls_loss", l2, "pr fs up loss", up)
            # print("pr fs up loss", pr," ", fs," ",up)
            # sum_acc.append(
            avg_loss = np.mean(sum_loss)
            print("Mean loss train: ", avg_loss)
            self.saver.save(self.sess, save_path=self.current_save_folder + 'model' + str(i) + '.ckpt')
            self.saver.save(self.sess, save_path=self.current_save_folder + 'model.ckpt')

            loss_valid = self.valid(self.batch_size)
            print("loss_valid", loss_valid)
        print("stop_train")

    def test(self, batch_size, file=None):
        # neu truyen vao tham so file load model len
        if file is not None:
            self.sess.close()
            self.sess = tf.InteractiveSession()
            self.saver.restore(self.sess, self.current_save_folder + file)
        img_test = []
        label_test = []
        with open("./data_full/big_data_process/data_landmark.pickle","r") as out_file:
            data_lm = pickle.load(out_file)

        with open("./data_full/big_data_process/name_file_test.txt","r") as out_file:
            file_names = json.load(out_file)
        file_names = file_names['name']
        # file_names = file_names[:100]

        number_image= len(file_names)

        number_batch =int(number_image /self.batch_size)
        print("number_image_test", number_image)
        sum_loss= []

        for i in range(number_batch):
            top = int(i * (batch_size))
            bottom = min((i + 1) * batch_size, number_image)
            img_batch, label_batch = read_image.read_image(file_names[top:bottom], data_lm)
            img_batch = np.asarray(img_batch)
            label_batch = np.asarray(label_batch)
            # img_batch , label_batch = Input_estimantor.augumentation(batch_x= img_batch, batch_y= label_batch)
            ttl = self.sess.run([self.total_loss],
                                feed_dict={self._x: img_batch, self._y: label_batch, self.is_traning: False})
            sum_loss.append(ttl)
            print("batch ", i, "loss ", ttl)
        loss = np.mean(sum_loss)
        print("Loss test", loss)

    def valid(self, batch_size):
        with open("./data_full/big_data_process/data_landmark.pickle","r") as out_file:
            data_lm = pickle.load(out_file)

        with open("./data_full/big_data_process/name_file_valid.txt","r") as out_file:
            file_names = json.load(out_file)
        # file_names =file_names[:100]
        number_image= len(file_names)
        number_batch =int(number_image /    self.batch_size)
        sum_loss = []
        for j in range(number_batch):
            top = int(j * (self.batch_size))
            bottom = int(min((j + 1) * (self.batch_size), number_image))
            while (bottom - top < self.batch_size):
                top = top - 1
            img_batch, label_batch = read_image.read_image(file_names[top:bottom], data_lm)
            img_batch = np.asarray(img_batch)
            label_batch = np.asarray(label_batch)
            ttl = self.sess.run([self.total_loss],
                                   feed_dict={self._x: img_batch, self._y: label_batch, self.is_traning: False})
            sum_loss.append(ttl)
        avg_loss = np.mean(sum_loss)
        return avg_loss
    def draw(self,img,label ):
        img = np.array([img[0]])
        label = np.array([label[0]])
        print(label.shape)
        sc , pr, fu, res , hm= self.sess.run([self.score_upsample,self.score_primary, self.score_fusion, self.res_map_batch, self.heat_map_primary], feed_dict={self._x: img, self._y: label, self.is_traning: False})
        sc = np.transpose(sc, axes=[0, 3, 1, 2])
        pr = np.transpose(pr, axes=[0,3,1,2])
        fu = np.transpose(fu, axes=[0,3,1,2])
        res = np.transpose(res, axes=[0,3,1,2])
        hm = np.transpose(hm, axes=[0,3,1,2])
        b = sc[0][0]
        a = res[0][0]
        for i in range(68):
             img[0][int(label[0][i][1])][int(label[0][i][0])] = 1.0
        plt.imshow(img[0])
        plt.show()
        # plt.imshow(sc[0][0])
        # plt.show()
        # plt.imshow(sc[0][1])
        # plt.show()
        # plt.imshow(sc[0][2])
        # plt.show()
        # plt.imshow(sc[0][3])
        # plt.show()
        # plt.imshow(sc[0][4])
        # plt.show()
        # plt.imshow(sc[0][5])
        # plt.show()
        # plt.imshow(sc[0][10])
        # plt.show()
        # plt.imshow(sc[0][11])
        # plt.show()
        # plt.imshow(sc[0][15])
        # plt.show()
        # plt.imshow(sc[0][20])
        # plt.show()
        # plt.imshow(sc[0][30])
        # plt.show()
        # plt.imshow(sc[0][40])
        # plt.show()
        # plt.imshow(sc[0][50])
        # plt.show()
        # plt.imshow(sc[0][60])
        # plt.show()
        # plt.imshow(sc[0][32])
        # plt.show()
        # plt.imshow(sc[0][35])
        # plt.show()
        # plt.imshow(sc[0][45])
        # plt.show()
        # plt.imshow(sc[0][48])
        # plt.show()
        # assert False
        plt.imshow(res[0][50])
        plt.show()
        plt.imshow(sc[0][50])
        plt.show()
        plt.imshow(res[0][60])
        plt.show()
        plt.imshow(sc[0][60])
        plt.show()
        plt.imshow(res[0][0])
        plt.show()
        plt.imshow(sc[0][0])
        plt.show()
        plt.imshow(res[0][10])
        plt.show()
        plt.imshow(sc[0][10])
        plt.show()
        plt.imshow(res[0][20])
        plt.show()
        plt.imshow(sc[0][20])
        plt.show()
        plt.imshow(pr[0][0])
        plt.show()
        plt.imshow(pr[0][10])
        plt.show()
        plt.imshow(fu[0][0])
        plt.show()
        plt.imshow(fu[0][10])
        plt.show()



    def peak_map(self, data):
        img = np.array([data[0][0]])
        label = np.array([data[0][1]])
        print(label.shape)
        sc , res = self.sess.run([self.score_upsample, self.res_map_batch], feed_dict={self._x: img, self._y: label, self.is_traning: False})
        print(np.array(sc).shape)
        sc = np.transpose(sc, axes=[0, 3, 1, 2])
        res = np.transpose(res, axes=[0,3,1,2])

        b = res[0]
        result = []
        b_copy = b
        for i in range(68):
            a = b[i]
            row = np.argmax(a, axis=1)
            x = a[np.arange(len(a)), row.squeeze()]
            cow = np.argmax(x, axis=0)
            result.append([cow, row[cow]])
        print(label)
        return result


# train_data  = read_data.readata("./data_full/300W_process/data_full_test.pickle")
# #asser False
# net = FCN_net( train_data = train_data, batch_size = 1, epoch = 3 )
# pixel = net.peak_map(train_data)
# print(pixel)
# assert False

# train_data = read_data.readata("./data_full/300W_process/data_full_test.pickle")
# test_data = read_data.readata("./data_full/300W_process/data_full_test.pickle")
# valid_data = test_data[:30][:][:][:]
# test_data = test_data[30:][:][:][:

# with open("./data_full/big_data_process/data_landmark.pickle","r") as out_file:
#      data_lm = pickle.load(out_file)
# with open("./data_full/big_data_process/name_file_test.txt", "r") as out_file:
#      file_names = json.load(out_file)
# file_names = file_names['name']
# file_names = file_names[20:21]
# img, label  = read_image.read_image(file_names,data_lm )
# net = FCN_net( batch_size = 1, epoch = 3 )
# pixel = net.draw(img, label)
# assert False

         # draw landmark
# for i in range(4):
#     img[i][int(label[i][20][1])][int(label[i][20][0])] = 1.0
# plt.imshow(img[0])
# plt.show()
# plt.imshow(img[1])
# plt.show()
# plt.imshow(img[2])
# plt.show()
# plt.imshow(img[3])
# plt.show()
# assert False
# print(img)



net = FCN_net( batch_size=2, epoch=7)
net.train(num_epoch=7)
net.test(batch_size=2)

